#ifndef PARAMS_H 
#define PARAMS_H

#define PLAN_TOPIC "plan"

#define MAX_STEPS_NAME "max_steps"
#define MAX_STEPS_DEF 0

#define DIAGONAL_MOVE_NAME "move_diagonal"
#define DIAGONAL_MOVE_DEF false

#define RESULT_TOPIC_NAME "move_base_result"
#define RESULT_TOPIC_DEF "/move_base/result"

#define MAP_UP_TOPIC_NAME "map_updates_topic"
#define MAP_UP_TOPIC_DEF "/move_base/global_costmap/costmap_updates"

#endif
