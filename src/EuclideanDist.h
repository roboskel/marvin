#ifndef EUCLIDEAN_DIST_H 
#define EUCLIDEAN_DIST_H 

#include"Heuristic.h"
#include<rosconsole/macros_generated.h>

#define COST_NEUTRAL 50
namespace RosDStar
{
class EuclideanDist :public RosDStar::Heuristic
{
    public:
        EuclideanDist(){};
        
        virtual cost_t h(CellPtr from, CellPtr to)
        {
            float f=(float)from->getX();
            f-=to->getX();
            
            float x2=(float)from->getX();
            x2-=to->getX();
            x2=pow(x2,2.0);
            
            float y2=(float)from->getY();
            y2-=to->getY();
            y2=pow(y2,2.0);
            
            x2+=y2;
            x2=sqrt(x2) * costNeutral;
            return (cost_t) x2;            
        }
};
}

#endif
