#ifndef KEY_H 
#define KEY_H

#include "types.h"

namespace RosDStar
{
    
class Key
{
    public:
        Key(unsigned int k1,unsigned int k2):first(k1),second(k2){};  

        bool operator ==(const Key &other) const
        {
            return keyCmp(*this,other)==0;
        }
        bool operator  >(const Key &other) const
        {
            return keyCmp(*this,other)>0;
        }
        bool operator  <(const Key &other) const
        {
            return keyCmp(*this,other)<0;
        }        
        
        cost_t first;
        cost_t second;
        
        /*
         * like strcmp
         * 
         * return -1 if k1<k2
         * return 0 for equality
         * return 1 if k1>k2
         */
        static char keyCmp(const Key &k1,const Key &k2)        
        {
            if(k1.first<k2.first)
                return -1;
            if(k1.first>k2.first)
                return 1;
            if(k1.second<k2.second)
                return -1;
            if(k1.second>k2.second)
                return 1;
            
            return 0;
            
        }
};
}

#endif
