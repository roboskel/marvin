#include"LiteDStar.h"

#include <utility>  

#define MAX(a,b) a>b?a:b
#define MIN(a,b) a<b?a:b

#define DEBUG(s) ROS_DEBUG(s)
#include <pthread.h>

RosDStar::LiteDStar::LiteDStar ( RosDStar::Heuristic* h )
{
    heuristic=h;
    max_steps=0;
    key_modifier=0;
}


bool RosDStar::LiteDStar::findPath ()
{    
    
    if(!start)
    {
        DEBUG("Not valid start point");
        return false;
    }
    
    if(!goal)
    {
        DEBUG("Not valid goal point");
        return false;
    }    
        
    goal->setRhs(0);
    Key gkey=calcKey(goal);
    goal->setKey(gkey);
    goal->setNeedUpdate(false);            
    insertToOpenList(goal);    
    bool b=computeShortestPath();
    return b;
}

void RosDStar::LiteDStar::getPath ( RosDStar::CellVec& path ) const
{        
    path.clear();
    if(!start || start->getG()==inf)
    {
        return ;
    }

    CellPtr cell=start;
    CellVec succVec;

    cost_t costSum=0;
    
    while(cell!=goal)
    {
        path.insert(path.end(),cell);
        cmap->getSucc(cell,succVec);
        
        cost_t cmin=inf;
        cost_t cost=0;
        CellPtr nextCell;
        for(CellVecIt it=succVec.begin();it!=succVec.end();it++)
        {
            CellPtr succ=*it;
            if(succ->getG()==inf)
                continue;

            cost_t c=cmap->getCost(cell,succ)+succ->getG();
            if(cmin>c)
            {
                cmin=c;
                nextCell=succ;
                cost=cmap->getCost(cell,succ);
            }
        }
        costSum +=cost;
        
        if(cmin==inf||costSum>start->getG() )
        {
            path.clear();
            return;
        }  
        cell=nextCell;

    }
    path.insert(path.end(),goal);
}


bool RosDStar::LiteDStar::updatePath(RosDStar::CellVec &updateCells)
{        
    if(!start)
    {
        ROS_ERROR("not valid start point");
        return false;
    }    
    
    for(CellVec::iterator it=updateCells.begin();it!=updateCells.end();it++)
    {        
        CellVec pred;
        cmap->getPred(*it,pred);
        for(CellVec::iterator predIt=pred.begin();predIt!=pred.end();predIt++)
        {
            updateVertex(*predIt);
        }
        updateVertex(*it);
    }
    updateCells.clear();//clear for memory efficient
        
    bool b=computeShortestPath();
    return b;
}

bool RosDStar::LiteDStar::computeShortestPath()
{    
    openList_t::iterator it=openList.begin();           
    uint step=0;
    while( it!=openList.end() )        
    {
        CellPtr c=*it;     
        Key startKey=calcKey(start);
        
        if(c->getKey()>startKey && start->getRhs() == start->getG() )
        {
            break ;
        }
        step++;
        
        Key k_old=c->getKey();
        removeFromOpenList(c);
        Key newKey=calcKey(c);
        c->setKey(newKey);
        
        if( k_old < newKey )
        {
            insertToOpenList(c);
        }
        else if(c->getG()>c->getRhs())
        {            
            c->setG( c->getRhs() );
            CellVec v;
            cmap->getPred(c,v);
            for(CellVecIt it=v.begin();it!=v.end();it++)
            {
                updateVertex(*it);
            }
        }
        else
        {
            c->setG(inf);
            CellVec v;
            cmap->getPred(c,v);
            for(CellVecIt it=v.begin();it!=v.end();it++)
            {
                updateVertex(*it);
            }
            updateVertex(c);            
        }
        it=openList.begin();
        
        if(max_steps!=0 && step>max_steps)
            return false;    
    }
    return start->getG()!=inf;
}

void RosDStar::LiteDStar::updateVertex ( CellPtr c )
{
    if(c==goal || cmap->occupied(c))
    {
        return;
    }
    
    CellVec succ;
    cmap->getSucc(c,succ);    
    cost_t min_rhs=inf;    
    for(CellVecIt it=succ.begin();it!=succ.end();it++)
    {
        CellPtr ptr=*it;
        cost_t rhs;
        if(ptr->getG()!=inf)
        {
            rhs=cmap->getCost(c,ptr) + ptr->getG();
        }
        else
        {
            rhs=inf;
        }
        
        if(rhs<min_rhs)
        {
            min_rhs=rhs;
        }
    }    
    c->setRhs(min_rhs);
    
    if( c->isOnOpenList() )
    {
        removeFromOpenList(c);
    }    
    if(c->getG()!=c->getRhs() )
    {
        Key key=calcKey(c);
        c->setKey(key);
        insertToOpenList(c);
    }
    c->setNeedUpdate(false);
}

void RosDStar::LiteDStar::setStart ( CellPtr start )
{
    if(!start)
    {
        ROS_ERROR("Start point is not valid");
        this->start.reset();
        return ;
    }
    
    if(cmap->occupied(start) )
    {
        ROS_ERROR("Start point is occupied");
        this->start.reset();
        return ;
    }
    
    if(this->start)
    {
        key_modifier+=heuristic->h(this->start,start);
    }
    
    this->start=start;
}

void RosDStar::LiteDStar::setStart(uint x, uint y)
{
    setStart( cmap->getCellPtr(x,y) );
}


void RosDStar::LiteDStar::setGoal (uint x,uint y )
{
    setGoal( cmap->getCellPtr(x,y) );
}

void RosDStar::LiteDStar::setGoal ( RosDStar::CellPtr goal )
{
    if(!goal)
    {
        ROS_ERROR("Goal point is not valid");
        this->goal.reset();
        return;
    }
    
    if(cmap->occupied(goal) )
    {
        ROS_ERROR("Goal point is occupied.");
        this->goal.reset();
        return ;
    }
    
    this->goal=goal;
}

void RosDStar::LiteDStar::reset()
{
    ROS_DEBUG("Reseting lite D*");
    goal.reset();
    start.reset();
    openList.clear();
    cmap->reset();
    key_modifier=0;
}


void RosDStar::LiteDStar::insertToOpenList ( RosDStar::CellPtr c )
{
    openList_t::iterator it=openList.insert(c);
    c->setOnOpenList(true);
    c->setCellIter(it);
}

void RosDStar::LiteDStar::removeFromOpenList ( CellPtr c )
{
    std::set<CellPtr>::iterator it=c->getCellIter();
    c->setCellIter(openList.end());
    c->setOnOpenList(false);
    openList.erase(it);
}


RosDStar::Key RosDStar::LiteDStar::calcKey ( CellPtr c ) const
{
    Key key(0,0);    
    key.second=MIN(c->getG(),c->getRhs() );    
    
    key.first=key.second;    
    if(key.first!=inf)
    {
        key.first += heuristic->h(start,c) +key_modifier ;
    }        
    return key;
}

