#include"CellMap.h"

#include"Cell.h"

#define COST_OBS_ROS 253        // ROS values of 253 are obstacles

RosDStar::CellMap::CellMap ( costmap_2d::Costmap2DROS* costmap_ros,bool moveDiag )
:CellMapBase(costmap_ros,moveDiag)
{
 
}


RosDStar::cost_t RosDStar::CellMap::getCost (const CellPtr &c1 ,const CellPtr &c2) const
{
    if(occupied(c2))
    {
        return inf;
    }

    cost_t c;
    if(moveDiag)
    {        
        if(c1->getX()!=c2->getX() &&  //if diagonal movement
            c1->getY()!=c2->getY() )
        {
            c=M_SQRT2*costNeutral;
        }
        else
        {
            c=costNeutral;
        }
    }
    else
    {
        c=costNeutral;
    }
    
    return c+c2->getRosCost()*costFactor;
}

bool RosDStar::CellMap::occupied ( const CellPtr c ) const
{    
    if(c->getRosCost()>=COST_OBS_ROS)
    {
        return true;
    }
    return false;
}

void RosDStar::CellMap::updateWindow ( uint sx,uint sy, uint width,uint height,CellVec &updateCells)
{   
    for (uint y = 0; y < height ; y++)
    {            
        for (uint x = 0; x < width ; x++)
        {
            uint dx,dy;
            dx=sx + x;
            dy=sy + y;
            
            CellPtr ptr=getCellPtr(dx,dy,false);
            
            if(ptr)
            {
                unsigned char cost=costMapRos->getCostmap()->getCost(dx,dy);
                if(cost!=ptr->getRosCost() && !ptr->getNeedUpdate() )
                {
                    updateCells.push_back(ptr);
                    ptr->setRosCost(cost);
                    ptr->setNeedUpdate(true);
                }
            }
        }
    }
}

