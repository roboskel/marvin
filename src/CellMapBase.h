#ifndef CELLMAP_BASE_H 
#define CELLMAP_BASE_H

#include"types.h"
#include"Cell.h"

#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h> 

namespace RosDStar
{    

class CellMapBase
{    
    public:
        CellMapBase(costmap_2d::Costmap2DROS* costmap_ros,bool moveDiag=false);
        virtual ~CellMapBase();
        
        virtual void getSucc (const CellPtr &c, CellVec& v ) const;
        virtual void getPred (const CellPtr &c, CellVec& v ) const;
        
        virtual cost_t getCost (const CellPtr &c1 ,const CellPtr &c2) const=0;
        virtual bool occupied(const CellPtr c) const=0;
                
        virtual void reset();

        /*
         * Return a Cell pointer from the hasmap.
         * If insert and there is no such pointer on the hasmap it creates one
         */
        virtual CellPtr getCellPtr(unsigned int x,unsigned int y,bool insert=true) const;
        virtual CellPtr getCellPtrFromIndex ( unsigned int index, bool insert ) const;
        virtual void updateWindow ( uint sx,uint sy, uint width,uint height,CellVec &updateCells)=0;   
        
    protected:
        costmap_2d::Costmap2DROS *costMapRos;
        mutable boost::unordered_map<unsigned int,CellPtr> hashMap; 
        bool moveDiag;
};

};

#endif
