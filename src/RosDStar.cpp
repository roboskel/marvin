#include"RosDStar.h"
#include <pluginlib/class_list_macros.h>

#include"Cell.h"
#include"EuclideanDist.h"
#include"Manhattandis.h"
#include"DiagonalDist.h"
#include"CellMap.h"
#include"params.h"
#include<nav_msgs/Path.h>




#define COST_NEUTRAL 50       // Set this to "open space" value
#define COST_FACTOR 3        // Used for translating costs 
#define COST_FACTOR_DIAG 1

#define MEASURE_TIME

PLUGINLIB_EXPORT_CLASS(RosDStar::RosLiteDStar, nav_core::BaseGlobalPlanner)

timespec RosDStar::RosLiteDStar::diffTime(timespec start, timespec end) const
{
  timespec temp;
        if ((end.tv_nsec-start.tv_nsec)<0) {
                temp.tv_sec = end.tv_sec-start.tv_sec-1;
                temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
        } else {
                temp.tv_sec = end.tv_sec-start.tv_sec;
                temp.tv_nsec = end.tv_nsec-start.tv_nsec;
        }
        return temp;
}

void RosDStar::RosLiteDStar::initialize ( std::string name, costmap_2d::Costmap2DROS* costmap_ros )
{
    
    ROS_DEBUG("Lite D star initialization\n");
    
    if(hasInitialized)
        return;    
    
    ros::NodeHandle node("~/" + name);
    int max_steps;
    std::string result_topic;
    bool moveDiag;
            
    node.param(MAX_STEPS_NAME,max_steps,MAX_STEPS_DEF);
    node.param(RESULT_TOPIC_NAME,result_topic,std::string(RESULT_TOPIC_DEF)  );
    node.param(MAP_UP_TOPIC_NAME,map_up_topic,std::string(MAP_UP_TOPIC_DEF) );
    node.param(DIAGONAL_MOVE_NAME,moveDiag,DIAGONAL_MOVE_DEF);
    
    //TODO add parameter
    useGradient=true;
    
    costMap2d=costmap_ros;
    cm=new CellMap(costmap_ros,moveDiag);

    Heuristic *h;
    if(moveDiag)
    {
        h=new DiagonalDist();            
        cm->setCostFactor(COST_FACTOR_DIAG);
    
    }
    else
    {
        h=new Manhhattandis();     
        cm->setCostFactor(COST_FACTOR);
    }
            
    h->setCostNeutral(COST_NEUTRAL);
    
    dStar=new LiteDStar(h);
    cm->setCostNeutral(COST_NEUTRAL);
    
    dStar->setCellMap(cm);
    hasInitialized=true;
    
    publ= node.advertise<nav_msgs::Path>(PLAN_TOPIC, 100);    
    dStar->setMaxSteps((uint)max_steps);    
    
    resultSub=node.subscribe(result_topic,10,&RosDStar::RosLiteDStar::resultCallback,this);
}

RosDStar::RosLiteDStar::~RosLiteDStar()
{
    if(!hasInitialized)
        return ;
   
    Heuristic *h=dStar->getHeuristic();
    CellMap *cm=dStar->getCellMap();
    
    delete h;
    delete cm;
    delete dStar;
}


bool RosDStar::RosLiteDStar::makePlan ( const geometry_msgs::PoseStamped& start, const geometry_msgs::PoseStamped& goal, std::vector< geometry_msgs::PoseStamped >& plan )
{
    plan.clear();  
 
    if(!hasInitialized)
    {
        ROS_ERROR("Planner has not initialized.");       
        return false;
    }

    if (goal.header.frame_id != costMap2d->getGlobalFrameID())
    {
        ROS_ERROR("This planner as configured will only accept goals in the %s frame, but a goal was sent in the %s frame.",
                costMap2d->getGlobalFrameID().c_str(), goal.header.frame_id.c_str());
        return false;
    }
    
    unsigned int startX,startY,goalX,goalY;
//     double startX,startY,goalX,goalY;
    
     getCordinates(start.pose,startX,startY);
     getCordinates(goal.pose,goalX,goalY);        
    
//     worldToMap(start.pose.position.x,start.pose.position.y,startX,startY);
//     worldToMap(goal.pose.position.x,goal.pose.position.y,goalX,goalY);
    
#ifdef MEASURE_TIME    
    timespec time1, time2;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID , &time1);
#endif
        
    bool pathFound=false;
    if(newGoal(goalX,goalY))
    {
        mutex.lock();
        updateCells.clear();
        dStar->reset();
        dStar->setGoal((uint)goalX,(uint)goalY);
        dStar->setStart((uint)startX,(uint)startY);
        pathFound=dStar->findPath();           
        mutex.unlock();
        
        connectToUpdates();
    }    
    else
    {
        mutex.lock();
        dStar->setStart(startX,startY);
        pathFound=dStar->updatePath(updateCells);    
        mutex.unlock();
    }
    /*
    else if(updateCells.size()>0 )
    {
        mutex.lock();
        dStar->setStart(startX,startY);
        pathFound=dStar->updatePath(updateCells);    
        mutex.unlock();
    }
    else //there are no updates. send the previous plan.
    {
        plan=lastPlan;
        publishPlan(lastPlan);
        return true;
    }
    */
    if(!pathFound)
    {
        ROS_WARN("No path found");
        return false;
    }
    
    CellVec cellPath;    
    mutex.lock();
    dStar->getPath(cellPath);
    mutex.unlock();
    
    if(cellPath.size()==0)
    {
        ROS_WARN("A path with zero size found!!");
        return false;
    }
    
    if(useGradient)
    {
        if(!gradientPlan(startX,startY,goalX,goalY,goal,plan) )
            ROS_ERROR("NO PATH!!!");
    }
    else
    {
        tf::Stamped < tf::Pose > start_tf;    
        poseStampedMsgToTF(start, start_tf);
        planFromCells(cellPath,goal,plan);
    }        

#ifdef MEASURE_TIME    
    clock_gettime(CLOCK_THREAD_CPUTIME_ID , &time2);   
    ROS_DEBUG("TIME %ld:%ld\n",diffTime(time1,time2).tv_sec , (diffTime(time1,time2).tv_nsec) );
#endif
    
    publishPlan(plan);
    lastPlan=plan;
    return true;
}

void RosDStar::RosLiteDStar::planFromCells ( const RosDStar::CellVec& v, geometry_msgs::PoseStamped goal, std::vector< geometry_msgs::PoseStamped >& plan ) const
{
    plan.clear();
    ros::Time plan_time = ros::Time::now();
    for(uint i=0;i<v.size()-1;i++)
    {
        geometry_msgs::PoseStamped pose=goal;
        const CellPtr &c=v[i];
        
        double x,y;        
        convertToCoordinate(c,x,y);
        pose.header.stamp = plan_time;
        pose.header.frame_id = goal.header.frame_id;
        pose.pose.position.x=x;
        pose.pose.position.y=y;
        pose.pose.position.z=0;
        pose.pose.orientation.x=0;
        pose.pose.orientation.y=0;
        pose.pose.orientation.z=0;
        pose.pose.orientation.w=1;
        plan.push_back(pose);
    }
    plan.push_back(goal);
}

void RosDStar::RosLiteDStar::publishPlan(std::vector< geometry_msgs::PoseStamped >& plan)
{        
    nav_msgs::Path gui_path;
    gui_path.poses.resize(plan.size());

    if(!plan.empty())
    {
      gui_path.header.frame_id = plan[0].header.frame_id;
//       gui_path.header.stamp = plan[0].header.stamp;
      gui_path.header.stamp =ros::Time::now();
    }

    // Extract the plan in world co-ordinates, we assume the path is all in the same frame
    for(unsigned int i=0; i < plan.size(); i++){
      gui_path.poses[i] = plan[i];
    }
    
    publ.publish(gui_path);
}

RosDStar::RosLiteDStar::RosLiteDStar ( std::string name, costmap_2d::Costmap2DROS* costmap_ros )
{
    ROS_DEBUG("RosLiteDStar allocated with arguments\n");
    hasInitialized=false;
    initialize(name,costmap_ros);
}

RosDStar::RosLiteDStar::RosLiteDStar()
{
    ROS_DEBUG("RosLiteDStar allocated without arguments");
    hasInitialized=false;
    costMap2d=0;
    dStar=0;
    cm=0;
//     convert_offset_=0.5;
   convert_offset_=0.0;
}

void RosDStar::RosLiteDStar::getCordinates(const geometry_msgs::Pose& pose, unsigned int& x, unsigned int& y) const
{
    double poseX,poseY;
    poseX=pose.position.x;
    poseY=pose.position.y;
    costMap2d->getCostmap()->worldToMap(poseX,poseY,x,y);  
}

void RosDStar::RosLiteDStar::convertToCoordinate(CellPtr c, double& x, double& y) const
{
    costMap2d->getCostmap()->mapToWorld(c->getX(),c->getY(),x,y);  
}

void RosDStar::RosLiteDStar::connectToUpdates()
{
    ros::NodeHandle node;
    mapUpdatesSub=node.subscribe(map_up_topic,1,&RosDStar::RosLiteDStar::mapUpdated,this);
}

void RosDStar::RosLiteDStar::disconnectFromUpdates()
{
    mapUpdatesSub.shutdown();
}

void RosDStar::RosLiteDStar::mapUpdated(const map_msgs::OccupancyGridUpdateConstPtr& update)
{    
    ROS_DEBUG("map updates");
    if(!mutex.try_lock() )
        return ;
    
#ifdef MEASURE_TIME    
    timespec time1, time2;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID , &time1);
#endif
    ROS_DEBUG("UP (%d,%d)",update->width,update->height);
    cm->updateWindow(update->x,update->y,update->width,update->height,updateCells);
    
#ifdef MEASURE_TIME    
    clock_gettime(CLOCK_THREAD_CPUTIME_ID , &time2);   
    ROS_DEBUG("UPDATTIME %ld:%ld\n",diffTime(time1,time2).tv_sec , (diffTime(time1,time2).tv_nsec) );
#endif
    mutex.unlock();
}

void RosDStar::RosLiteDStar::resultCallback(const move_base_msgs::MoveBaseActionResult& result)
{
    
    
    using namespace actionlib_msgs;
    unsigned char status=result.status.status;
    
    ROS_DEBUG("%s",result.status.text.c_str());
    
    if(status==GoalStatus::PREEMPTED ||
        status==GoalStatus::SUCCEEDED ||
        status==GoalStatus::ABORTED ||
        status==GoalStatus::REJECTED ||
        status==GoalStatus::RECALLED )
    {
        mutex.lock();
        dStar->reset();
        updateCells.clear();
        disconnectFromUpdates();
        mutex.unlock();
    }
}

void RosDStar::RosLiteDStar::mapToWorld(double mx, double my, double& wx, double& wy) {
    wx = costMap2d->getCostmap()->getOriginX() + (mx+convert_offset_) * costMap2d->getCostmap()->getResolution();
    wy = costMap2d->getCostmap()->getOriginY() + (my+convert_offset_) * costMap2d->getCostmap()->getResolution();
}

bool RosDStar::RosLiteDStar::worldToMap(double wx, double wy, double& mx, double& my) {
    double origin_x = costMap2d->getCostmap()->getOriginX();
    double origin_y = costMap2d->getCostmap()->getOriginY();
    double resolution = costMap2d->getCostmap()->getResolution();

    if (wx < origin_x || wy < origin_y)
        return false;

    mx = (wx - origin_x) / resolution - convert_offset_;
    my = (wy - origin_y) / resolution - convert_offset_;

    if (mx < costMap2d->getCostmap()->getSizeInCellsX() && my < costMap2d->getCostmap()->getSizeInCellsY())
        return true;

    return false;
}


bool RosDStar::RosLiteDStar::gradientPlan(double start_x, 
                                          double start_y, 
                                          double goal_x, 
                                          double goal_y,
                                          const geometry_msgs::PoseStamped& goal,
                                          std::vector<geometry_msgs::PoseStamped>& plan)
{

    //clear the plan, just in case
    plan.clear();
    std::vector<std::pair<float, float> > path;
    
    PathMaker pm(cm);
    int nx = costMap2d->getCostmap()->getSizeInCellsX();
    int ny = costMap2d->getCostmap()->getSizeInCellsY();
    pm.setSize(nx,ny);
    
//     uint start_x,start_y,goal_x,goal_y;
//     costMap2d->getCostmap()->worldToMap(sx, sy, start_x, start_y);
//     costMap2d->getCostmap()->worldToMap(gx, gy, goal_x, goal_y);
    
    if (!pm.makePath(start_x, start_y, goal_x, goal_y, path)) {
        ROS_ERROR("NO PATH!");
        return false;
    }

    ros::Time plan_time = ros::Time::now();
//     for (int i = path.size() -1; i>=0; i--) {
    for (uint i = 0;i<path.size();i++) {
        std::pair<float, float> point = path[i];
        //convert the plan to world coordinates
        double world_x, world_y;
//         mapToWorld(point.first, point.second, world_x, world_y);
//         costMap2d->getCostmap()->mapToWorld(point.first, point.second, world_x, world_y); 
        mapToWorld(point.first, point.second, world_x, world_y); 

        geometry_msgs::PoseStamped pose;
        pose.header.stamp = plan_time;
        pose.header.frame_id = goal.header.frame_id;
        pose.pose.position.x = world_x;
        pose.pose.position.y = world_y;
        pose.pose.position.z = 0.0;
        pose.pose.orientation.x = 0.0;
        pose.pose.orientation.y = 0.0;
        pose.pose.orientation.z = 0.0;
        pose.pose.orientation.w = 1.0;
        plan.push_back(pose);
    }
    return !plan.empty();
}
