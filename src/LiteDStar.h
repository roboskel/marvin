#ifndef Lite_D_STAR_H
#define Lite_D_STAR_H

#include <vector>
#include <set>

#include "Cell.h"
#include"Heuristic.h"
#include"types.h"
#include"CellMap.h"
#include"Key.h"

namespace RosDStar
{
    


class LiteDStar
{
    public:
        LiteDStar(Heuristic *h);
        ~LiteDStar(){};
        
        bool findPath();        
        bool updatePath(RosDStar::CellVec &updateCells);
        void getPath(CellVec &v) const;
        
        void setStart(CellPtr start);
        void setStart(uint x,uint y );
        void setGoal(uint x,uint y );
        void setGoal(CellPtr goal);
        
        CellPtr getGoal() const
        {
            return goal;
        }
        
        CellPtr getStart() const
        {
            return start;
        }
        
        void reset();
        
        CellMap* getCellMap() const
        {
            return cmap;
        }
        
        void setCellMap(CellMap *map)
        {
            cmap=map;
        }
        
        Heuristic* getHeuristic() const
        {
            return heuristic;
        }
        
        void setMaxSteps(uint steps)
        {
            max_steps=steps;
        }
        
        uint getMaxSteps() const
        {
            return max_steps;
        }
        
    private:                
        void updateVertex(CellPtr v);       
        bool computeShortestPath();        
        
        void insertToOpenList(CellPtr c);
        void removeFromOpenList(CellPtr c);        
        
        Key calcKey(CellPtr c) const;

        Heuristic *heuristic;
        CellMap *cmap;              
        uint max_steps;       
        
        CellPtr start;
        CellPtr goal;
        cost_t key_modifier;
                
        struct CellCmp
        {
            bool operator() (const CellPtr c1, const CellPtr c2) const
            {
                return c1->getKey()<c2->getKey();
            }
        };
        
        typedef std::multiset<CellPtr,CellCmp > openList_t;
        openList_t openList;
};
    
};


#endif
