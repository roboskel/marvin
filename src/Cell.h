#ifndef CELL_H 
#define CELL_H

#include <sys/types.h>
#include <vector>
#include <set>
#include "types.h"
#include <stdlib.h>  
#include"Key.h"
namespace RosDStar
{

    
class Cell
{
    public:
        Cell(unsigned int x,unsigned int y);
        ~Cell();
        
        unsigned int getX() const
        {
            return x;
        }
        
        unsigned int getY() const
        {
            return y;
        }
        
        void setCellIter(std::set<CellPtr>::iterator it)
        {
            cellIter=it;
        }
        
        std::set<CellPtr>::iterator getCellIter()
        {
            return cellIter;
        }
        
        bool isOnOpenList() const
        {
            return onOpenList;
        }
        
        void setOnOpenList(bool b)
        {
            onOpenList=b;
        }
               
        void setG(cost_t g)
        {
            this->g=g;
        }
        
        cost_t getG() const
        {
            return g;
        }
        
        void setRhs(cost_t rhs)
        {
            this->rhs=rhs;
        }
        
        cost_t getRhs() const
        {
            return rhs;
        }
        
        void setKey(Key key)
        {
            this->key=key;
        }
        
        void setKey(cost_t k1,cost_t k2)
        {
            this->key=Key(k1,k2);
        }
        
        Key getKey() const
        {
            return key;
        }
        
        bool operator ==(const Cell& c2) const;
        bool operator !=(const Cell& c2) const;
        
        unsigned char getRosCost() const
        {
            return rosCost;
        }
        
        void setRosCost(unsigned char c)
        {
            rosCost=c;
        }
        
        bool getNeedUpdate() const
        {
            return updated;
        }
        
        void setNeedUpdate(bool b)
        {
            updated=b;
        }
        
     

    private:        
        unsigned int x;
        unsigned int y;        
        unsigned char rosCost;
        cost_t rhs;
        cost_t g;
        Key key;
        
        //cell's position on open list
        std::set<CellPtr>::iterator cellIter;
        bool onOpenList;
        bool updated;
};

};

#endif
