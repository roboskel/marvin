#include"CellMapBase.h" 

RosDStar::CellMapBase::CellMapBase ( costmap_2d::Costmap2DROS* costmap_ros,bool moveDiag )
{
    costMapRos=costmap_ros;    
    this->moveDiag=moveDiag;
}

RosDStar::CellMapBase::~CellMapBase()
{
    hashMap.clear();
}


void RosDStar::CellMapBase::getSucc ( const CellPtr &c, CellVec& v ) const
{    
    if(occupied(c) )
    {
        v.clear();
        return;
    }
    
    getPred(c,v);
}

void RosDStar::CellMapBase::getPred (const CellPtr &c, CellVec& v ) const
{
    v.clear();
    costmap_2d::Costmap2D *cmap2D=costMapRos->getCostmap();    
    
    
    if(moveDiag)
    {     
        v.reserve(8);        
        uint minX,maxX,minY,maxY;
        
        maxX=std::min(c->getX()+2,cmap2D->getSizeInCellsX() );
        if(c->getX()!=0)
        {
            minX=c->getX()-1;
        }
        else
        {
            minX=0;
        }        
            
        maxY=std::min(c->getY()+2,cmap2D->getSizeInCellsY() );
        if(c->getY()!=0)
        {
            minY=c->getY()-1;
        }
        else
        {
            minY=0;
        }            
        
        for(uint y=minY;y<maxY;y++)
        {
            for(uint x=minX;x<maxX ; x++)
            {
                CellPtr pred=getCellPtr(x,y);
                if(pred!=c && !occupied(pred) )
                {
                    v.push_back(pred);
                }
            }
        }
    
    }
    else
    {
        v.reserve(4);
        
        if(c->getX()+1 <cmap2D->getSizeInCellsX())
        {
            CellPtr r=getCellPtr( c->getX()+1,c->getY() );
            if(!occupied(r) )
                v.push_back(r);
        }
        if(c->getX() > 0)
        {
            CellPtr l=getCellPtr( c->getX()-1,c->getY() );                        
            if(!occupied(l) )
                v.push_back(l);
        }
        if(c->getY()+1 < cmap2D->getSizeInCellsY())
        {
            CellPtr u=getCellPtr( c->getX(),c->getY()+1 );
            if(!occupied(u) )
                v.push_back(u);
            
        }
        if(c->getY()>0)
        {
            CellPtr d=getCellPtr( c->getX(),c->getY()-1 );                    
            if(!occupied(d) )
                v.push_back(d);
        }
    }
}


RosDStar::CellPtr RosDStar::CellMapBase::getCellPtr ( uint x,uint y,bool insert) const
{
    uint index=costMapRos->getCostmap()->getIndex(x,y);
    CellPtr ret;    
    try
    {
        ret=hashMap.at(index);
    }
    catch(std::out_of_range) //Cell does not exist in the hash map lets add it
    {
        if(insert)
        {
            ret=CellPtr(new Cell(x,y) );
            unsigned char cost=costMapRos->getCostmap()->getCost(x,y);
            ret->setRosCost(cost);
            std::pair<unsigned int,CellPtr> in(index, ret);
            hashMap.insert(in);
        }
    }       
    return ret;
}

void RosDStar::CellMapBase::reset()
{
    hashMap.clear();
}

RosDStar::CellPtr RosDStar::CellMapBase::getCellPtrFromIndex ( unsigned int index, bool insert ) const
{
    CellPtr ret; 
    try
    {
        ret=hashMap.at(index);
    }
    catch(std::out_of_range) //Cell does not exist in the hash map lets add it
    {
        CellPtr nullPtr;
        return nullPtr;
    }
    return ret;
}

