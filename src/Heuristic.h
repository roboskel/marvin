#ifndef HEURISTIC_H 
#define HEURISTIC_H

#include"Cell.h"

namespace RosDStar
{

class Heuristic
{
    public:
        Heuristic():costNeutral(1){};
        virtual ~Heuristic(){}
        
        virtual cost_t h(CellPtr from, CellPtr to)=0;
        void setCostNeutral(cost_t c)
        {
            costNeutral=c;
        }
        
        cost_t getCostNeutral()
        {
            return costNeutral;
        }
    protected:
        cost_t costNeutral;
};

};
#endif
