#ifndef TYPES_H 
#define TYPES_H

#include <limits>
#include <vector>
#include <utility>  
#include <boost/shared_ptr.hpp>

namespace RosDStar
{

class Cell;
// typedef  unsigned int cost_t;
typedef  float cost_t;
typedef unsigned int uint;
const cost_t inf=std::numeric_limits<cost_t>::max();

typedef boost::shared_ptr<Cell> CellPtr;
typedef boost::shared_ptr<Cell const> CellConstPtr;

typedef std::vector<CellPtr> CellVec;
typedef std::vector<CellPtr>::iterator CellVecIt;
typedef std::vector<CellPtr>::const_iterator CellVecConstIt;

};



#endif
