#ifndef CELL_MAP_H 
#define CELL_MAP_H

#include"types.h"
#include"Cell.h"
#include "CellMapBase.h"

#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h> 
#include<boost/unordered_map.hpp>
#include <boost/thread.hpp>
#include<string.h>

namespace RosDStar
{    

class CellMap :public CellMapBase
{    
    public:
        CellMap(costmap_2d::Costmap2DROS* costmap_ros,bool moveDiag=false);                     
        cost_t getCost (const CellPtr &c1 ,const CellPtr &c2) const;
        bool occupied(const CellPtr c) const;
                        
        cost_t getCostNeutral() const
        {
            return costNeutral;
        }
        
        void setCostNeutral(cost_t c)
        {
            costNeutral=c;
        }
        
        cost_t getCostFactor() const
        {
            return costFactor;
        }
        
        void setCostFactor(cost_t c)
        {
            costFactor=c;
        }   
        
       void updateWindow ( uint sx,uint sy, uint width,uint height,CellVec &updateCells);   
        
    private:
      
        cost_t costNeutral;
        cost_t costFactor;
};

};
#endif
