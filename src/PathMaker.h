#ifndef PATH_MAKER_H
#define PATH_MAKER_H
#include "CellMapBase.h"
#include <utility>
namespace RosDStar
{

class PathMaker
{
    public:
        PathMaker(CellMapBase *c) :cellMap(c),lethal_cost_(253), pathStep_(0.5),gradx_(0),grady_(0)  {}
        ~PathMaker()
        {
            if(gradx_!=0)
                delete gradx_;
            if(grady_!=0)
                delete grady_;
        }
        
        bool makePath( double start_x, 
                       double start_y, 
                       double goal_x,
                       double goal_y, std::vector<std::pair<float, float> >& path);
        
        inline uint getIndex(int x, int y) 
        {
            return x + y * xs_;
        }
        
        void setSize(int x,int y)
        {
            xs_=x;
            ys_=y;
                
            if (gradx_)
                delete[] gradx_;
            if (grady_)
                delete[] grady_;
            
            gradx_ = new float[x * y];
            grady_ = new float[x * y];
        }
        
    private:
        float gradCell( int n);
        float potential(uint intex);
        
        CellMapBase *cellMap;
        int lethal_cost_;
        float pathStep_; /**< step size for following gradient */                
        float *gradx_, *grady_; /**< gradient arrays, size of potential array */
        int xs_, ys_;
       
        
};

}

#endif