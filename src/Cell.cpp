#include"Cell.h" 
 #include <ros/console.h>
 
RosDStar::Cell::Cell (unsigned int x,unsigned int y )  :key(inf,inf)
{
    this->x=x;
    this->y=y;
    rhs=inf;
    g=inf;
    onOpenList=false;
    updated=false;
}

bool RosDStar::Cell::operator== ( const RosDStar::Cell &c2 ) const
{
    return x==c2.x && y==c2.y;
}

bool RosDStar::Cell::operator!= ( const RosDStar::Cell &c2 ) const
{
    return  x!=c2.x || y!= c2.y;
}

RosDStar::Cell::~Cell()
{

}

