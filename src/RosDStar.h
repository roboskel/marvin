#ifndef ROS_LITE_D_STAR_H
#define ROS_LITE_D_STAR_H 

#include <ros/ros.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h> 
#include <nav_core/base_global_planner.h>
#include <geometry_msgs/PoseStamped.h>
#include <angles/angles.h>
#include <base_local_planner/world_model.h>
#include <base_local_planner/costmap_model.h>
#include <time.h>
#include<move_base/move_base.h>

#include"PathMaker.h"
#include"LiteDStar.h"



namespace RosDStar
{
    

    
class RosLiteDStar :public nav_core::BaseGlobalPlanner
{
    public:
        RosLiteDStar();
        ~RosLiteDStar();
        RosLiteDStar(std::string name, costmap_2d::Costmap2DROS* costmap_ros);
        
        void initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros);

        bool makePlan(const geometry_msgs::PoseStamped& start, 
                const geometry_msgs::PoseStamped& goal, 
                std::vector<geometry_msgs::PoseStamped>& plan
               ); 
        
    private:
        void getCordinates(const geometry_msgs::Pose& start,unsigned int &x,unsigned int &y) const;
        void convertToCoordinate(CellPtr c, double& x, double& y) const;
        void publishPlan(std::vector< geometry_msgs::PoseStamped >& plan);
        
        ros::Publisher publ;
        ros::Subscriber mapUpdatesSub;
        ros::Subscriber resultSub;
        
        LiteDStar *dStar;
        CellMap *cm;
        costmap_2d::Costmap2DROS *costMap2d;
        bool hasInitialized;
                
        void planFromCells(const CellVec &v,geometry_msgs::PoseStamped p, std::vector<geometry_msgs::PoseStamped>& plan) const;
//         bool moveDiag;          
        bool useGradient;
        float convert_offset_;
        
        std::string map_up_topic;
        std::vector< geometry_msgs::PoseStamped> lastPlan;
        CellVec updateCells;
        
        
            
        void connectToUpdates();        
        void disconnectFromUpdates();
        void mapUpdated ( const map_msgs::OccupancyGridUpdateConstPtr& update );    
        void resultCallback(const move_base_msgs::MoveBaseActionResult &result);
        
        //diff time for 
        timespec diffTime(timespec start, timespec end) const;                        
        inline bool newGoal(uint x,uint y);
        boost::mutex mutex;
        
        bool worldToMap(double wx, double wy, double& mx, double& my) ;
        void mapToWorld(double mx, double my, double& wx, double& wy);
        
        bool gradientPlan(double start_x, 
                    double start_y,                           
                    double goal_x,                           
                    double goal_y,                    
                    const geometry_msgs::PoseStamped& goal,                    
                    std::vector<geometry_msgs::PoseStamped>& plan);
};


bool RosLiteDStar::newGoal(uint x, uint y)
{
    if(!dStar->getGoal())
        return true;
    
    if(dStar->getGoal()->getX()!=x ||
        dStar->getGoal()->getY()!=y )
    {
        return true;
    }
    
    return false;
}



};
#endif
