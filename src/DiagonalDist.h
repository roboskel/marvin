#include"Heuristic.h"

#define MAX(a,b) a>b?a:b
#define MIN(a,b) a<b?a:b

namespace RosDStar
{
  
class DiagonalDist :public Heuristic
{
    public:
        DiagonalDist(){};
        virtual cost_t h(CellPtr from, CellPtr to)
        {            
            unsigned int dx,dy;
            
            if(from->getX()>to->getX() )
            {
                dx=from->getX()-to->getX();
            }
            else
            {
                dx=to->getX()-from->getX();
            }
            
            if(from->getY()>to->getY() )
            {
                dy=from->getY()-to->getY();
            }
            else
            {
                dy=to->getY()-from->getY();
            }
            
            int min=std::min(dx,dy);
            int max=std::max(dx,dy);
            cost_t ret=(cost_t)M_SQRT2*min+(max-min);
            ret*=costNeutral;
            return ret;
        }

};
}
