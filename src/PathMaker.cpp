#include"PathMaker.h"
#include<string.h>
#include<stdio.h>

#define POT_HIGH 1.0e10

bool RosDStar::PathMaker::makePath ( double start_x, 
                                     double start_y, 
                                     double goal_x,
                                     double goal_y, std::vector< std::pair< float, float > >& path )
{
    std::pair<float, float> current;
//     int stc = getIndex(goal_x, goal_y);
    int stc = getIndex(start_x, start_y);

    // set up offset
//     float dx = goal_x - (int)goal_x;
//     float dy = goal_y - (int)goal_y;
    
    float dx = start_x - (int)start_x;
    float dy = start_y - (int)start_y;
    int ns = xs_ * ys_;
    
    gradx_=new float[ns];
    grady_=new float[ns];
//     memset(gradx_, 0, ns * sizeof(float));
//     memset(grady_, 0, ns * sizeof(float));

    int c = 0;
    while (c++<ns*4) {
        // check if near goal
        double nx = stc % xs_ + dx, ny = stc / xs_ + dy;

//          if (fabs(nx - start_x) < .5 && fabs(ny - start_y) < .5) {
        if (fabs(nx - goal_x) < .5 && fabs(ny - goal_y) < .5) {
//             current.first = start_x;
//             current.second = start_y;
            current.first = goal_x;
            current.second = goal_y;
            path.push_back(current);
            return true;
        }

        if (stc < xs_ || stc > xs_ * ys_ - xs_) // would be out of bounds
        {
            ROS_DEBUG("[PathCalc] Out of bounds\n");
            return false;
        }

        current.first = nx;
        current.second = ny;

        //ROS_INFO("%d %d | %f %f ", stc%xs_, stc/xs_, dx, dy);

        path.push_back(current);

        bool oscillation_detected = false;
        int npath = path.size();
        if (npath > 2 && path[npath - 1].first == path[npath - 3].first
                && path[npath - 1].second == path[npath - 3].second) {
//             ROS_DEBUG("[PathCalc] oscillation detected, attempting fix.");
            oscillation_detected = true;
        }

        int stcnx = stc + xs_;
        int stcpx = stc - xs_;

        // check for potentials at eight positions near cell
        if (potential(stc) >= POT_HIGH || potential(stc + 1) >= POT_HIGH || potential(stc - 1) >= POT_HIGH
                || potential(stcnx) >= POT_HIGH || potential(stcnx + 1) >= POT_HIGH || potential(stcnx - 1) >= POT_HIGH
                || potential(stcpx) >= POT_HIGH || potential(stcpx + 1) >= POT_HIGH || potential(stcpx - 1) >= POT_HIGH
                || oscillation_detected) {
//             ROS_DEBUG("[Path] Pot fn boundary, following grid (%0.1f/%d)", potential(stc), (int) path.size());
            // check eight neighbors to find the lowest
            int minc = stc;
            int minp = potential(stc);
            int st = stcpx - 1;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st++;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st++;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st = stc - 1;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st = stc + 1;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st = stcnx - 1;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st++;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            st++;
            if (potential(st) < minp) {
                minp = potential(st);
                minc = st;
            }
            stc = minc;
            dx = 0;
            dy = 0;

            //ROS_DEBUG("[Path] Pot: %0.1f  pos: %0.1f,%0.1f",
            //    potential(stc), path[npath-1].first, path[npath-1].second);

            if (potential(stc) >= POT_HIGH) {
                ROS_DEBUG("[PathCalc] No path found, high potential");
                //savemap("navfn_highpot");
                return 0;
            }
        }

        // have a good gradient here
        else {

            // get grad at four positions near cell
            gradCell(stc);
            gradCell(stc + 1);
            gradCell(stcnx);
            gradCell(stcnx + 1);

            // get interpolated gradient
            float x1 = (1.0 - dx) * gradx_[stc] + dx * gradx_[stc + 1];
            float x2 = (1.0 - dx) * gradx_[stcnx] + dx * gradx_[stcnx + 1];
            float x = (1.0 - dy) * x1 + dy * x2; // interpolated x
            float y1 = (1.0 - dx) * grady_[stc] + dx * grady_[stc + 1];
            float y2 = (1.0 - dx) * grady_[stcnx] + dx * grady_[stcnx + 1];
            float y = (1.0 - dy) * y1 + dy * y2; // interpolated y

            // show gradients
            ROS_DEBUG(
                    "[Path] %0.2f,%0.2f  %0.2f,%0.2f  %0.2f,%0.2f  %0.2f,%0.2f; final x=%.3f, y=%.3f\n", gradx_[stc], grady_[stc], gradx_[stc+1], grady_[stc+1], gradx_[stcnx], grady_[stcnx], gradx_[stcnx+1], grady_[stcnx+1], x, y);

            // check for zero gradient, failed
            if (x == 0.0 && y == 0.0) {
                ROS_DEBUG("[PathCalc] Zero gradient");
                return 0;
            }

            // move in the right direction
            float ss = pathStep_ / hypot(x, y);
            dx += x * ss;
            dy += y * ss;

            // check for overflow
            if (dx > 1.0) {
                stc++;
                dx -= 1.0;
            }
            if (dx < -1.0) {
                stc--;
                dx += 1.0;
            }
            if (dy > 1.0) {
                stc += xs_;
                dy -= 1.0;
            }
            if (dy < -1.0) {
                stc -= xs_;
                dy += 1.0;
            }

        }

        //printf("[Path] Pot: %0.1f  grad: %0.1f,%0.1f  pos: %0.1f,%0.1f\n",
        //         potential(stc), dx, dy, path[npath-1].first, path[npath-1].second);
    }

    return false;
}

float RosDStar::PathMaker::potential ( RosDStar::uint intex )
{
    CellPtr ptr=cellMap->getCellPtrFromIndex(intex,false);
    if(!ptr)
    {
        return POT_HIGH;
    }
    
    return ptr->getG();
}


float RosDStar::PathMaker::gradCell (int n )
{
    if (gradx_[n] + grady_[n] > 0.0)    // check this cell
        return 1.0;

    if (n < xs_ || n > xs_ * ys_ - xs_)    // would be out of bounds
        return 0.0;
    float cv = potential(n);
    float dx = 0.0;
    float dy = 0.0;

    // check for in an obstacle
    if (cv >= POT_HIGH) {
        if (potential(n - 1) < POT_HIGH)
            dx = -lethal_cost_;
        else if (potential(n + 1) < POT_HIGH)
            dx = lethal_cost_;

        if (potential(n - xs_) < POT_HIGH)
            dy = -lethal_cost_;
        else if (potential(xs_ + 1) < POT_HIGH)
            dy = lethal_cost_;
    }

    else                // not in an obstacle
    {
        // dx calc, average to sides
        if (potential(n - 1) < POT_HIGH)
            dx += potential(n - 1) - cv;
        if (potential(n + 1) < POT_HIGH)
            dx += cv - potential(n + 1);

        // dy calc, average to sides
        if (potential(n - xs_) < POT_HIGH)
            dy += potential(n - xs_) - cv;
        if (potential(n + xs_) < POT_HIGH)
            dy += cv - potential(n + xs_);
    }

    // normalize
    float norm = hypot(dx, dy);
    if (norm > 0) {
        norm = 1.0 / norm;
        gradx_[n] = norm * dx;
        grady_[n] = norm * dy;
    }
    return norm;
}