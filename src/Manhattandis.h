#ifndef MANHATTAN_DIST_H
#define MANHATTAN_DIST_H

#include"Heuristic.h"

namespace RosDStar
{

class Manhhattandis :public Heuristic
{
    public:
        Manhhattandis(){}
        virtual cost_t h(CellPtr from, CellPtr to)
        {
//             #define H(cell) (abs((cell)->y - mazegoal->y) + abs((cell)->x - mazegoal->x))
            cost_t h=0;
            
            if(from->getY() >to->getY() )
            {
                h=from->getY() -to->getY();
            }
            else
            {
                h=to->getY() -from->getY();
            }
            
            if(from->getX() >to->getX() )
            {
                h+=from->getX() - to->getX();                
            }
            else
            {
                h+=to->getX() - from->getX();                
            }
            
//             h=std::abs(from->getY()-to->getY()) + std::abs(from->getX() - to->getX() );
            return h*costNeutral;
        }
};

    
};
#endif